/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

/**
 *
 * @author Mi
 */
public class OyenteVentana implements ActionListener {
    

    JFrame ventana;
    public OyenteVentana(JFrame ventana) {
        this.ventana=ventana;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        acercaDe();
    }
    
    public  void acercaDe() {                          
        // TODO add your handling code here:
        DialogoAcercaDe dialogo = new DialogoAcercaDe(ventana,true);
        dialogo.setSize(500,400);
        int ancho = ventana.getWidth();
        int alto = ventana.getHeight();
        int dAncho = dialogo.getWidth();
        dialogo.setLocation(ventana.getX()+(ancho-dAncho)/2,ventana.getY()+100);
        dialogo.setVisible(true);
    }  
    
}
