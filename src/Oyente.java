
import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Set;
import javax.swing.JButton;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Mi
 */
public class Oyente implements ActionListener,KeyListener {

    private Pizarra panel;

    public Oyente(Pizarra panel) {

        this.panel = panel;

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton origen = (JButton) e.getSource();

        if (origen.getName().equals("escalar")) {

            System.out.println("Escalar");
            int n = (Integer.parseInt(panel.textoNumeroFigura.getText())) - 1;

            Figura f = panel.aFiguras.get(n);

            if (f instanceof Linea) {
                Linea l = (Linea) f;
                panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, panel.panelRaster.getBackground(), false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                double esc = Double.parseDouble(panel.textoEscalar.getText());

                transX = ((int) l.punto1.getX() - panel.oX) * (-1);
                transY = ((int) l.punto1.getY() - panel.oY) * (-1);

                punto[0] = l.punto1.getX();
                punto[1] = l.punto1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = l.punto1.getX() - panel.oX;
                punto[1] = l.punto1.getY() - panel.oY;
                punto[2] = 1;
                m.escalar(esc);
                m.traslacion(transX * (-1), transY * (-1));
                punto2 = m.pprima(punto);
                l.punto1.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                punto[0] = l.punto2.getX();
                punto[1] = l.punto2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = l.punto2.getX() - panel.oX;
                punto[1] = l.punto2.getY() - panel.oY;
                punto[2] = 1;
                m1.escalar(esc);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                l.punto2.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                //panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, l.color, false);
                panel.dibujarFiguras();

            }

            if (f instanceof TrianguloR) {
                TrianguloR t = (TrianguloR) f;
                Point p1 = new Point();
                Point p2 = new Point();
                Point p3 = new Point();
                p1.setLocation(t.v[0].x, t.v[0].y);
                p2.setLocation(t.v[1].x, t.v[1].y);
                p3.setLocation(t.v[2].x, t.v[2].y);
                panel.dibujarTriangulo(p1, p2, p3, panel.panelRaster.getBackground(), false);

                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                double esc = Double.parseDouble(panel.textoEscalar.getText());

                transX = ((int) p1.x - panel.oX) * (-1);
                transY = ((int) p1.y - panel.oY) * (-1);

                punto[0] = p1.getX();
                punto[1] = p1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = p1.getX() - panel.oX;
                punto[1] = p1.getY() - panel.oY;
                punto[2] = 1;
                m.escalar(esc);
                m.traslacion(transX * (-1), transY * (-1));
                punto2 = m.pprima(punto);
                p1.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                punto[0] = p2.getX();
                punto[1] = p2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = p2.getX() - panel.oX;
                punto[1] = p2.getY() - panel.oY;
                punto[2] = 1;
                m1.escalar(esc);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                p2.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                punto[0] = p3.getX();
                punto[1] = p3.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY);
                punto[0] = p3.getX() - panel.oX;
                punto[1] = p3.getY() - panel.oY;
                punto[2] = 1;
                m2.escalar(esc);
                m2.traslacion(transX * (-1), transY * (-1));
                punto2 = m2.pprima(punto);
                p3.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                Vertex2D v1 = new Vertex2D(p1.x, p1.y, t.color.getRGB());
                Vertex2D v2 = new Vertex2D(p2.x, p2.y, t.color.getRGB());
                Vertex2D v3 = new Vertex2D(p3.x, p3.y, t.color.getRGB());

                TrianguloR tri = new TrianguloR(v1, v2, v3, t.color);
                panel.aFiguras.set(n, tri);
                //     panel.dibujarTriangulo(p1, p2, p3, t.color, false);
                panel.dibujarFiguras();
            }

            if (f instanceof Circulo) {
                Circulo cr = (Circulo) f;
                panel.dibujarCirculo((Point) cr.puntoC, (Point) cr.puntoR, panel.panelRaster.getBackground(), false);
                System.out.println(cr.puntoC.getX() + "," + cr.puntoR.getX());
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                double esc = Double.parseDouble(panel.textoEscalar.getText());

                transX = ((int) cr.puntoC.getX() - panel.oX) * (-1);
                transY = ((int) cr.puntoC.getY() - panel.oY) * (-1);

                punto[0] = cr.puntoC.getX();
                punto[1] = cr.puntoC.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = cr.puntoC.getX() - panel.oX;
                punto[1] = cr.puntoC.getY() - panel.oY;
                punto[2] = 1;
                m.escalar(esc);
                m.traslacion(transX * (-1), transY * (-1));
                punto2 = m.pprima(punto);
                cr.puntoC.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                punto[0] = cr.puntoR.getX();
                punto[1] = cr.puntoR.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = cr.puntoR.getX() - panel.oX;
                punto[1] = cr.puntoR.getY() - panel.oY;
                punto[2] = 1;
                m1.escalar(esc);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                cr.puntoR.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                panel.dibujarFiguras();
            }

            if (f instanceof Cuadrado) {
                Cuadrado cd = (Cuadrado) f;
                panel.dibujarTriangulo(cd.punto1, cd.punto2, cd.punto3,panel.panelRaster.getBackground(), false);
                panel.dibujarTriangulo(cd.punto1, cd.punto2, cd.punto4,panel.panelRaster.getBackground(), false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                int pxm = (int) ((int) cd.punto1.getX() + cd.punto2.getX()) / 2;
                int pym = (int) ((int) cd.punto1.getY() + cd.punto2.getY()) / 2;;
                double escalar = Double.parseDouble(panel.textoEscalar.getText());

                transX = (pxm - panel.oX) * (-1);
                transY = (pym - panel.oY) * (-1);

                punto[0] = cd.punto1.getX();
                punto[1] = cd.punto1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = cd.punto1.getX() - panel.oX;
                punto[1] = cd.punto1.getY() - panel.oY;
                punto[2] = 1;
                m.escalar(escalar);
                m.traslacion(transX * (-1), transY * (-1));
                punto2 = m.pprima(punto);
                cd.punto1.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                punto[0] = cd.punto2.getX();
                punto[1] = cd.punto2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = cd.punto2.getX() - panel.oX;
                punto[1] = cd.punto2.getY() - panel.oY;
                punto[2] = 1;
                m1.escalar(escalar);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                cd.punto2.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);
                
                punto[0] = cd.punto3.getX();
                punto[1] = cd.punto3.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY);
                punto[0] = cd.punto3.getX() - panel.oX;
                punto[1] = cd.punto3.getY() - panel.oY;
                punto[2] = 1;
                m2.escalar(escalar);
                m2.traslacion(transX * (-1), transY * (-1));
                punto2 = m2.pprima(punto);
                cd.punto3.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);
                
                punto[0] = cd.punto3.getX();
                punto[1] = cd.punto3.getY();
                punto[2] = 1;

                Matrix m3 = new Matrix();
                m3.traslacion(transX, transY);
                punto[0] = cd.punto4.getX() - panel.oX;
                punto[1] = cd.punto4.getY() - panel.oY;
                punto[2] = 1;
                m3.escalar(escalar);
                m3.traslacion(transX * (-1), transY * (-1));
                punto2 = m3.pprima(punto);
                cd.punto4.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                //    panel.dibujarCuadrado((Point) cd.punto1, (Point) cd.punto2, cd.color, false);
                panel.dibujarFiguras();
            }
            if (f instanceof Elipse) {
                Elipse el = (Elipse) f;
                panel.dibujarElipse((Point) el.puntoC, (Point) el.puntoR,(Point)el.puntoA, panel.panelRaster.getBackground(), false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                double esc = Double.parseDouble(panel.textoEscalar.getText());

                transX = ((int) el.puntoC.getX() - panel.oX) * (-1);
                transY = ((int) el.puntoC.getY() - panel.oY) * (-1);

                punto[0] = el.puntoC.getX();
                punto[1] = el.puntoC.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = el.puntoC.getX() - panel.oX;
                punto[1] = el.puntoC.getY() - panel.oY;
                punto[2] = 1;
                m.escalar(esc);
                m.traslacion(transX * (-1), transY * (-1));
                punto2 = m.pprima(punto);
                el.puntoC.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                punto[0] = el.puntoR.getX();
                punto[1] = el.puntoR.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = el.puntoR.getX() - panel.oX;
                punto[1] = el.puntoR.getY() - panel.oY;
                punto[2] = 1;
                m1.escalar(esc);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                el.puntoR.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);
                
                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY);
                punto[0] = el.puntoA.getX() - panel.oX;
                punto[1] = el.puntoA.getY() - panel.oY;
                punto[2] = 1;
                m2.escalar(esc);
                m2.traslacion(transX * (-1), transY * (-1));
                punto2 = m2.pprima(punto);
                el.puntoA.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                panel.dibujarFiguras();

            }

        } else if (origen.getName().equals("rotar") && panel.textoNumeroFigura.getText() != null) {

            System.out.println("Rotar");
            int n = Integer.parseInt(panel.textoNumeroFigura.getText()) - 1;
            Figura f = panel.aFiguras.get(n);

            if (f instanceof Linea) {
                Linea l = (Linea) f;
                panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, panel.panelRaster.getBackground(), false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                int pmx = (int) ((int) (l.punto1.getX() + l.punto2.getX())) / 2;
                int pmy = (int) ((int) (l.punto1.getY() + l.punto2.getY())) / 2;
                int rotar = Integer.parseInt(panel.textoRotar.getText());

                transX = (pmx - panel.oX) * (-1);
                transY = (pmy - panel.oY) * (-1);

                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = l.punto1.getX() - panel.oX;
                punto[1] = l.punto1.getY() - panel.oY;
                punto[2] = 1;
                m.rotacion(rotar);
                m.traslacion(transX * (-1), transY * (-1));
                punto2 = m.pprima(punto);
                l.punto1.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                punto[0] = l.punto2.getX();
                punto[1] = l.punto2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = l.punto2.getX() - panel.oX;
                punto[1] = l.punto2.getY() - panel.oY;
                punto[2] = 1;
                m1.rotacion(rotar);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                l.punto2.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                //    panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, l.color, false);
                panel.dibujarFiguras();
            }

            if (f instanceof TrianguloR) {
                TrianguloR t = (TrianguloR) f;
                Point p1 = new Point();
                Point p2 = new Point();
                Point p3 = new Point();
                p1.setLocation(t.v[0].x, t.v[0].y);
                p2.setLocation(t.v[1].x, t.v[1].y);
                p3.setLocation(t.v[2].x, t.v[2].y);
                panel.dibujarTriangulo(p1, p2, p3, panel.panelRaster.getBackground(), false);

                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                int pmx = (p1.x + p2.x + p3.x) / 3;
                int pmy = (p1.y + p2.y + p3.y) / 3;
                int rotar = Integer.parseInt(panel.textoRotar.getText());

                transX = (pmx - panel.oX) * (-1);
                transY = (pmy - panel.oY) * (-1);

                punto[0] = p1.getX();
                punto[1] = p1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = p1.getX() - panel.oX;
                punto[1] = p1.getY() - panel.oY;
                punto[2] = 1;
                m.rotacion(rotar);
                m.traslacion(transX * (-1), transY * (-1));
                punto2 = m.pprima(punto);
                p1.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                punto[0] = p2.getX();
                punto[1] = p2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = p2.getX() - panel.oX;
                punto[1] = p2.getY() - panel.oY;
                punto[2] = 1;
                m1.rotacion(rotar);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                p2.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                punto[0] = p3.getX();
                punto[1] = p3.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY);
                punto[0] = p3.getX() - panel.oX;
                punto[1] = p3.getY() - panel.oY;
                punto[2] = 1;
                m2.rotacion(rotar);
                m2.traslacion(transX * (-1), transY * (-1));
                punto2 = m2.pprima(punto);
                p3.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                Vertex2D v1 = new Vertex2D(p1.x, p1.y, t.color.getRGB());
                Vertex2D v2 = new Vertex2D(p2.x, p2.y, t.color.getRGB());
                Vertex2D v3 = new Vertex2D(p3.x, p3.y, t.color.getRGB());

                TrianguloR tri = new TrianguloR(v1, v2, v3, t.color);
                panel.aFiguras.set(n, tri);
                //    panel.dibujarTriangulo(p1, p2, p3, t.color, false);
                panel.dibujarFiguras();
            }

            if (f instanceof Circulo) {
                Circulo cr = (Circulo) f;
                panel.dibujarFiguras();
            }

            if (f instanceof Cuadrado) {
                Cuadrado cd = (Cuadrado) f;
                //panel.dibujarCuadrado((Point) cd.punto1, (Point) cd.punto2, Color.WHITE, false);
                panel.dibujarTriangulo(cd.punto1, cd.punto2, cd.punto3,panel.panelRaster.getBackground(), false);
                panel.dibujarTriangulo(cd.punto1, cd.punto2, cd.punto4,panel.panelRaster.getBackground(), false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                int pxm = (int) ((int) cd.punto1.getX() + cd.punto2.getX()) / 2;
                int pym = (int) ((int) cd.punto1.getY() + cd.punto2.getY()) / 2;;
                int rotar = Integer.parseInt(panel.textoRotar.getText());

                transX = (pxm - panel.oX) * (-1);
                transY = (pym - panel.oY) * (-1);

                punto[0] = cd.punto1.getX();
                punto[1] = cd.punto1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = cd.punto1.getX() - panel.oX;
                punto[1] = cd.punto1.getY() - panel.oY;
                punto[2] = 1;
                m.rotacion(rotar);
                m.traslacion(transX * (-1), transY * (-1));
                punto2 = m.pprima(punto);
                cd.punto1.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                punto[0] = cd.punto2.getX();
                punto[1] = cd.punto2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = cd.punto2.getX() - panel.oX;
                punto[1] = cd.punto2.getY() - panel.oY;
                punto[2] = 1;
                m1.rotacion(rotar);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                cd.punto2.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);
                
                punto[0] = cd.punto3.getX();
                punto[1] = cd.punto3.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY);
                punto[0] = cd.punto3.getX() - panel.oX;
                punto[1] = cd.punto3.getY() - panel.oY;
                punto[2] = 1;
                m2.rotacion(rotar);
                m2.traslacion(transX * (-1), transY * (-1));
                punto2 = m2.pprima(punto);
                cd.punto3.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);
                
                punto[0] = cd.punto3.getX();
                punto[1] = cd.punto3.getY();
                punto[2] = 1;

                Matrix m3 = new Matrix();
                m3.traslacion(transX, transY);
                punto[0] = cd.punto4.getX() - panel.oX;
                punto[1] = cd.punto4.getY() - panel.oY;
                punto[2] = 1;
                m3.rotacion(rotar);
                m3.traslacion(transX * (-1), transY * (-1));
                punto2 = m3.pprima(punto);
                cd.punto4.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                //    panel.dibujarCuadrado((Point) cd.punto1, (Point) cd.punto2, cd.color, false);
                panel.dibujarFiguras();
            }
            if (f instanceof Elipse) {
        /*        Elipse el = (Elipse) f;
                panel.dibujarElipse((Point) el.puntoC, (Point) el.puntoR,(Point)el.puntoA, Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX;
                int transY;
                transX = ((int) el.puntoC.getX() - panel.oX) * (-1);
                transY = ((int) el.puntoC.getY() - panel.oY) * (-1);

                int rotar = Integer.parseInt(panel.textoRotar.getText());


                punto[0] = el.puntoC.getX();
                punto[1] = el.puntoC.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY);
                punto[0] = el.puntoC.getX() - panel.oX;
                punto[1] = el.puntoC.getY() - panel.oY;
                punto[2] = 1;
                m.rotacion(rotar);
                m.traslacion(transX * (-1), transY * (-1));
                punto2 = m.pprima(punto);
                el.puntoC.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);

                punto[0] = el.puntoR.getX();
                punto[1] = el.puntoR.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY);
                punto[0] = el.puntoR.getX() - panel.oX;
                punto[1] = el.puntoR.getY() - panel.oY;
                punto[2] = 1;
                m1.rotacion(rotar);
                m1.traslacion(transX * (-1), transY * (-1));
                punto2 = m1.pprima(punto);
                el.puntoR.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);
                
                punto[0] = el.puntoA.getX();
                punto[1] = el.puntoA.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY);
                punto[0] = el.puntoA.getX() - panel.oX;
                punto[1] = el.puntoA.getY() - panel.oY;
                punto[2] = 1;
                m2.rotacion(rotar);
                m2.traslacion(transX * (-1), transY * (-1));
                punto2 = m2.pprima(punto);
                el.puntoA.setLocation(punto2[0] + panel.oX, punto2[1] + panel.oY);
                */
               
 //               dibujarFiguras();
            }

        } else {

            System.out.println("Trasladar");
            int n = Integer.parseInt(panel.textoNumeroFigura.getText()) - 1;
            Figura f = panel.aFiguras.get(n);

            if (f instanceof Linea) {
                Linea l = (Linea) f;
                panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, panel.panelRaster.getBackground(), false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = Integer.parseInt(panel.textoTrasladarX.getText());
                int transY = Integer.parseInt(panel.textoTrasladarY.getText());

                punto[0] = l.punto1.getX();
                punto[1] = l.punto1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                l.punto1.setLocation(punto2[0], punto2[1]);

                punto[0] = l.punto2.getX();
                punto[1] = l.punto2.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                l.punto2.setLocation(punto2[0], punto2[1]);

                //  panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, l.color, false);
                panel.dibujarFiguras();
            }

            if (f instanceof TrianguloR) {
                TrianguloR t = (TrianguloR) f;
                Point p1 = new Point();
                Point p2 = new Point();
                Point p3 = new Point();
                p1.setLocation(t.v[0].x, t.v[0].y);
                p2.setLocation(t.v[1].x, t.v[1].y);
                p3.setLocation(t.v[2].x, t.v[2].y);
                panel.dibujarTriangulo(p1, p2, p3, Color.WHITE, false);

                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = Integer.parseInt(panel.textoTrasladarX.getText());
                int transY = Integer.parseInt(panel.textoTrasladarY.getText());

                punto[0] = p1.getX();
                punto[1] = p1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                p1.setLocation(punto2[0], punto2[1]);

                punto[0] = p2.getX();
                punto[1] = p2.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                p2.setLocation(punto2[0], punto2[1]);

                punto[0] = p3.getX();
                punto[1] = p3.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                p3.setLocation(punto2[0], punto2[1]);

                Vertex2D v1 = new Vertex2D(p1.x, p1.y, t.color.getRGB());
                Vertex2D v2 = new Vertex2D(p2.x, p2.y, t.color.getRGB());
                Vertex2D v3 = new Vertex2D(p3.x, p3.y, t.color.getRGB());

                TrianguloR tri = new TrianguloR(v1, v2, v3, t.color);
                panel.aFiguras.set(n, tri);
                //  panel.dibujarTriangulo(p1, p2, p3, t.color, false);
                panel.dibujarFiguras();
            }

            if (f instanceof Circulo) {
                Circulo cr = (Circulo) f;
                panel.dibujarCirculo((Point) cr.puntoC, (Point) cr.puntoR, Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = Integer.parseInt(panel.textoTrasladarX.getText());
                int transY = Integer.parseInt(panel.textoTrasladarY.getText());

                punto[0] = cr.puntoC.getX();
                punto[1] = cr.puntoC.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                cr.puntoC.setLocation(punto2[0], punto2[1]);

                punto[0] = cr.puntoR.getX();
                punto[1] = cr.puntoR.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                cr.puntoR.setLocation(punto2[0], punto2[1]);
                panel.dibujarFiguras();

            }

            if (f instanceof Cuadrado) {
                Cuadrado cd = (Cuadrado) f;
                panel.dibujarTriangulo(cd.punto1, cd.punto2, cd.punto3,Color.WHITE, false);
                panel.dibujarTriangulo(cd.punto1, cd.punto2, cd.punto4,Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = Integer.parseInt(panel.textoTrasladarX.getText());
                int transY = Integer.parseInt(panel.textoTrasladarY.getText());

                

                punto[0] = cd.punto1.getX();
                punto[1] = cd.punto1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY* (-1));
                punto2 = m.pprima(punto);
                cd.punto1.setLocation(punto2[0], punto2[1]);

                punto[0] = cd.punto2.getX();
                punto[1] = cd.punto2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY* (-1));
                punto2 = m1.pprima(punto);
                cd.punto2.setLocation(punto2[0], punto2[1] );
                
                punto[0] = cd.punto3.getX();
                punto[1] = cd.punto3.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY* (-1));
                punto2 = m2.pprima(punto);
                cd.punto3.setLocation(punto2[0] , punto2[1] );
                
                punto[0] = cd.punto4.getX();
                punto[1] = cd.punto4.getY();
                punto[2] = 1;

                Matrix m3 = new Matrix();
                m3.traslacion(transX, transY* (-1));
                punto2 = m3.pprima(punto);
                cd.punto4.setLocation(punto2[0] , punto2[1] );

                //    panel.dibujarCuadrado((Point) cd.punto1, (Point) cd.punto2, cd.color, false);
                panel.dibujarFiguras();
            }
            if (f instanceof Elipse) {
                Elipse el = (Elipse) f;
                panel.dibujarElipse((Point) el.puntoC, (Point) el.puntoR,(Point)el.puntoA, Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = Integer.parseInt(panel.textoTrasladarX.getText());
                int transY = Integer.parseInt(panel.textoTrasladarY.getText());
                
                

                punto[0] = el.puntoC.getX();
                punto[1] = el.puntoC.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY* (-1));
                punto2 = m.pprima(punto);
                el.puntoC.setLocation(punto2[0], punto2[1]);

                punto[0] = el.puntoR.getX();
                punto[1] = el.puntoR.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY* (-1));
                punto2 = m1.pprima(punto);
                el.puntoR.setLocation(punto2[0], punto2[1] );
                
                punto[0] = el.puntoA.getX();
                punto[1] = el.puntoA.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY* (-1));
                punto2 = m2.pprima(punto);
                el.puntoA.setLocation(punto2[0] , punto2[1] );
                
                panel.dibujarFiguras();
            }

        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
      int key = e.getKeyCode();
      if(key==KeyEvent.VK_DOWN){
      System.out.println("Trasladar");
            int n = Integer.parseInt(panel.textoNumeroFigura.getText()) - 1;
            Figura f = panel.aFiguras.get(n);

            if (f instanceof Linea) {
                Linea l = (Linea) f;
                panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, panel.panelRaster.getBackground(), false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = 0;
                int transY = -1;

                punto[0] = l.punto1.getX();
                punto[1] = l.punto1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                l.punto1.setLocation(punto2[0], punto2[1]);

                punto[0] = l.punto2.getX();
                punto[1] = l.punto2.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                l.punto2.setLocation(punto2[0], punto2[1]);

                //  panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, l.color, false);
                panel.dibujarFiguras();
            }

            if (f instanceof TrianguloR) {
                TrianguloR t = (TrianguloR) f;
                Point p1 = new Point();
                Point p2 = new Point();
                Point p3 = new Point();
                p1.setLocation(t.v[0].x, t.v[0].y);
                p2.setLocation(t.v[1].x, t.v[1].y);
                p3.setLocation(t.v[2].x, t.v[2].y);
                panel.dibujarTriangulo(p1, p2, p3, Color.WHITE, false);

                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = 0;
                int transY = -1;

                punto[0] = p1.getX();
                punto[1] = p1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                p1.setLocation(punto2[0], punto2[1]);

                punto[0] = p2.getX();
                punto[1] = p2.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                p2.setLocation(punto2[0], punto2[1]);

                punto[0] = p3.getX();
                punto[1] = p3.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                p3.setLocation(punto2[0], punto2[1]);

                Vertex2D v1 = new Vertex2D(p1.x, p1.y, t.color.getRGB());
                Vertex2D v2 = new Vertex2D(p2.x, p2.y, t.color.getRGB());
                Vertex2D v3 = new Vertex2D(p3.x, p3.y, t.color.getRGB());

                TrianguloR tri = new TrianguloR(v1, v2, v3, t.color);
                panel.aFiguras.set(n, tri);
                //  panel.dibujarTriangulo(p1, p2, p3, t.color, false);
                panel.dibujarFiguras();
            }

            if (f instanceof Circulo) {
                Circulo cr = (Circulo) f;
                panel.dibujarCirculo((Point) cr.puntoC, (Point) cr.puntoR, Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = 0;
                int transY = -1;

                punto[0] = cr.puntoC.getX();
                punto[1] = cr.puntoC.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                cr.puntoC.setLocation(punto2[0], punto2[1]);

                punto[0] = cr.puntoR.getX();
                punto[1] = cr.puntoR.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                cr.puntoR.setLocation(punto2[0], punto2[1]);
                panel.dibujarFiguras();

            }

            if (f instanceof Cuadrado) {
                Cuadrado cd = (Cuadrado) f;
                panel.dibujarTriangulo(cd.punto1, cd.punto2, cd.punto3,Color.WHITE, false);
                panel.dibujarTriangulo(cd.punto1, cd.punto2, cd.punto4,Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = 0;
                int transY = -1;

                

                punto[0] = cd.punto1.getX();
                punto[1] = cd.punto1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY* (-1));
                punto2 = m.pprima(punto);
                cd.punto1.setLocation(punto2[0], punto2[1]);

                punto[0] = cd.punto2.getX();
                punto[1] = cd.punto2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY* (-1));
                punto2 = m1.pprima(punto);
                cd.punto2.setLocation(punto2[0], punto2[1] );
                
                punto[0] = cd.punto3.getX();
                punto[1] = cd.punto3.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY* (-1));
                punto2 = m2.pprima(punto);
                cd.punto3.setLocation(punto2[0] , punto2[1] );
                
                punto[0] = cd.punto4.getX();
                punto[1] = cd.punto4.getY();
                punto[2] = 1;

                Matrix m3 = new Matrix();
                m3.traslacion(transX, transY* (-1));
                punto2 = m3.pprima(punto);
                cd.punto4.setLocation(punto2[0] , punto2[1] );

                //    panel.dibujarCuadrado((Point) cd.punto1, (Point) cd.punto2, cd.color, false);
                panel.dibujarFiguras();
            }
            if (f instanceof Elipse) {
                Elipse el = (Elipse) f;
                panel.dibujarElipse((Point) el.puntoC, (Point) el.puntoR,(Point)el.puntoA, Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = 0;
                int transY = -1;
                
                

                punto[0] = el.puntoC.getX();
                punto[1] = el.puntoC.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY* (-1));
                punto2 = m.pprima(punto);
                el.puntoC.setLocation(punto2[0], punto2[1]);

                punto[0] = el.puntoR.getX();
                punto[1] = el.puntoR.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY* (-1));
                punto2 = m1.pprima(punto);
                el.puntoR.setLocation(punto2[0], punto2[1] );
                
                punto[0] = el.puntoA.getX();
                punto[1] = el.puntoA.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY* (-1));
                punto2 = m2.pprima(punto);
                el.puntoA.setLocation(punto2[0] , punto2[1] );
                
                panel.dibujarFiguras();
            }
      }
      if(key==KeyEvent.VK_UP){
      System.out.println("Trasladar");
            int n = Integer.parseInt(panel.textoNumeroFigura.getText()) - 1;
            Figura f = panel.aFiguras.get(n);

            if (f instanceof Linea) {
                Linea l = (Linea) f;
                panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, panel.panelRaster.getBackground(), false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = 0;
                int transY = 1;

                punto[0] = l.punto1.getX();
                punto[1] = l.punto1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                l.punto1.setLocation(punto2[0], punto2[1]);

                punto[0] = l.punto2.getX();
                punto[1] = l.punto2.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                l.punto2.setLocation(punto2[0], punto2[1]);

                //  panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, l.color, false);
                panel.dibujarFiguras();
            }

            if (f instanceof TrianguloR) {
                TrianguloR t = (TrianguloR) f;
                Point p1 = new Point();
                Point p2 = new Point();
                Point p3 = new Point();
                p1.setLocation(t.v[0].x, t.v[0].y);
                p2.setLocation(t.v[1].x, t.v[1].y);
                p3.setLocation(t.v[2].x, t.v[2].y);
                panel.dibujarTriangulo(p1, p2, p3, Color.WHITE, false);

                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = 0;
                int transY = 1;

                punto[0] = p1.getX();
                punto[1] = p1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                p1.setLocation(punto2[0], punto2[1]);

                punto[0] = p2.getX();
                punto[1] = p2.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                p2.setLocation(punto2[0], punto2[1]);

                punto[0] = p3.getX();
                punto[1] = p3.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                p3.setLocation(punto2[0], punto2[1]);

                Vertex2D v1 = new Vertex2D(p1.x, p1.y, t.color.getRGB());
                Vertex2D v2 = new Vertex2D(p2.x, p2.y, t.color.getRGB());
                Vertex2D v3 = new Vertex2D(p3.x, p3.y, t.color.getRGB());

                TrianguloR tri = new TrianguloR(v1, v2, v3, t.color);
                panel.aFiguras.set(n, tri);
                //  panel.dibujarTriangulo(p1, p2, p3, t.color, false);
                panel.dibujarFiguras();
            }

            if (f instanceof Circulo) {
                Circulo cr = (Circulo) f;
                panel.dibujarCirculo((Point) cr.puntoC, (Point) cr.puntoR, Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = 0;
                int transY = 1;

                punto[0] = cr.puntoC.getX();
                punto[1] = cr.puntoC.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                cr.puntoC.setLocation(punto2[0], punto2[1]);

                punto[0] = cr.puntoR.getX();
                punto[1] = cr.puntoR.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                cr.puntoR.setLocation(punto2[0], punto2[1]);
                panel.dibujarFiguras();

            }

            if (f instanceof Cuadrado) {
                Cuadrado cd = (Cuadrado) f;
                panel.dibujarTriangulo(cd.punto1, cd.punto2, cd.punto3,Color.WHITE, false);
                panel.dibujarTriangulo(cd.punto1, cd.punto2, cd.punto4,Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = 0;
                int transY = 1;

                

                punto[0] = cd.punto1.getX();
                punto[1] = cd.punto1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY* (-1));
                punto2 = m.pprima(punto);
                cd.punto1.setLocation(punto2[0], punto2[1]);

                punto[0] = cd.punto2.getX();
                punto[1] = cd.punto2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY* (-1));
                punto2 = m1.pprima(punto);
                cd.punto2.setLocation(punto2[0], punto2[1] );
                
                punto[0] = cd.punto3.getX();
                punto[1] = cd.punto3.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY* (-1));
                punto2 = m2.pprima(punto);
                cd.punto3.setLocation(punto2[0] , punto2[1] );
                
                punto[0] = cd.punto4.getX();
                punto[1] = cd.punto4.getY();
                punto[2] = 1;

                Matrix m3 = new Matrix();
                m3.traslacion(transX, transY* (-1));
                punto2 = m3.pprima(punto);
                cd.punto4.setLocation(punto2[0] , punto2[1] );

                //    panel.dibujarCuadrado((Point) cd.punto1, (Point) cd.punto2, cd.color, false);
                panel.dibujarFiguras();
            }
            if (f instanceof Elipse) {
                Elipse el = (Elipse) f;
                panel.dibujarElipse((Point) el.puntoC, (Point) el.puntoR,(Point)el.puntoA, Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = 0;
                int transY = 1;
                
                

                punto[0] = el.puntoC.getX();
                punto[1] = el.puntoC.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY* (-1));
                punto2 = m.pprima(punto);
                el.puntoC.setLocation(punto2[0], punto2[1]);

                punto[0] = el.puntoR.getX();
                punto[1] = el.puntoR.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY* (-1));
                punto2 = m1.pprima(punto);
                el.puntoR.setLocation(punto2[0], punto2[1] );
                
                punto[0] = el.puntoA.getX();
                punto[1] = el.puntoA.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY* (-1));
                punto2 = m2.pprima(punto);
                el.puntoA.setLocation(punto2[0] , punto2[1] );
                
                panel.dibujarFiguras();
            }
      }
      if(key==KeyEvent.VK_LEFT){
      System.out.println("Trasladar");
            int n = Integer.parseInt(panel.textoNumeroFigura.getText()) - 1;
            Figura f = panel.aFiguras.get(n);

            if (f instanceof Linea) {
                Linea l = (Linea) f;
                panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, panel.panelRaster.getBackground(), false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = -1;
                int transY = 0;

                punto[0] = l.punto1.getX();
                punto[1] = l.punto1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                l.punto1.setLocation(punto2[0], punto2[1]);

                punto[0] = l.punto2.getX();
                punto[1] = l.punto2.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                l.punto2.setLocation(punto2[0], punto2[1]);

                //  panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, l.color, false);
                panel.dibujarFiguras();
            }

            if (f instanceof TrianguloR) {
                TrianguloR t = (TrianguloR) f;
                Point p1 = new Point();
                Point p2 = new Point();
                Point p3 = new Point();
                p1.setLocation(t.v[0].x, t.v[0].y);
                p2.setLocation(t.v[1].x, t.v[1].y);
                p3.setLocation(t.v[2].x, t.v[2].y);
                panel.dibujarTriangulo(p1, p2, p3, Color.WHITE, false);

                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = -1;
                int transY = 0;

                punto[0] = p1.getX();
                punto[1] = p1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                p1.setLocation(punto2[0], punto2[1]);

                punto[0] = p2.getX();
                punto[1] = p2.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                p2.setLocation(punto2[0], punto2[1]);

                punto[0] = p3.getX();
                punto[1] = p3.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                p3.setLocation(punto2[0], punto2[1]);

                Vertex2D v1 = new Vertex2D(p1.x, p1.y, t.color.getRGB());
                Vertex2D v2 = new Vertex2D(p2.x, p2.y, t.color.getRGB());
                Vertex2D v3 = new Vertex2D(p3.x, p3.y, t.color.getRGB());

                TrianguloR tri = new TrianguloR(v1, v2, v3, t.color);
                panel.aFiguras.set(n, tri);
                //  panel.dibujarTriangulo(p1, p2, p3, t.color, false);
                panel.dibujarFiguras();
            }

            if (f instanceof Circulo) {
                Circulo cr = (Circulo) f;
                panel.dibujarCirculo((Point) cr.puntoC, (Point) cr.puntoR, Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = -1;
                int transY = 0;

                punto[0] = cr.puntoC.getX();
                punto[1] = cr.puntoC.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                cr.puntoC.setLocation(punto2[0], punto2[1]);

                punto[0] = cr.puntoR.getX();
                punto[1] = cr.puntoR.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                cr.puntoR.setLocation(punto2[0], punto2[1]);
                panel.dibujarFiguras();

            }

            if (f instanceof Cuadrado) {
                Cuadrado cd = (Cuadrado) f;
                panel.dibujarTriangulo(cd.punto1, cd.punto2, cd.punto3,Color.WHITE, false);
                panel.dibujarTriangulo(cd.punto1, cd.punto2, cd.punto4,Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
              int transX = -1;
                int transY = 0;

                

                punto[0] = cd.punto1.getX();
                punto[1] = cd.punto1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY* (-1));
                punto2 = m.pprima(punto);
                cd.punto1.setLocation(punto2[0], punto2[1]);

                punto[0] = cd.punto2.getX();
                punto[1] = cd.punto2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY* (-1));
                punto2 = m1.pprima(punto);
                cd.punto2.setLocation(punto2[0], punto2[1] );
                
                punto[0] = cd.punto3.getX();
                punto[1] = cd.punto3.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY* (-1));
                punto2 = m2.pprima(punto);
                cd.punto3.setLocation(punto2[0] , punto2[1] );
                
                punto[0] = cd.punto4.getX();
                punto[1] = cd.punto4.getY();
                punto[2] = 1;

                Matrix m3 = new Matrix();
                m3.traslacion(transX, transY* (-1));
                punto2 = m3.pprima(punto);
                cd.punto4.setLocation(punto2[0] , punto2[1] );

                //    panel.dibujarCuadrado((Point) cd.punto1, (Point) cd.punto2, cd.color, false);
                panel.dibujarFiguras();
            }
            if (f instanceof Elipse) {
                Elipse el = (Elipse) f;
                panel.dibujarElipse((Point) el.puntoC, (Point) el.puntoR,(Point)el.puntoA, Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = -1;
                int transY = 0;
                
                

                punto[0] = el.puntoC.getX();
                punto[1] = el.puntoC.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY* (-1));
                punto2 = m.pprima(punto);
                el.puntoC.setLocation(punto2[0], punto2[1]);

                punto[0] = el.puntoR.getX();
                punto[1] = el.puntoR.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY* (-1));
                punto2 = m1.pprima(punto);
                el.puntoR.setLocation(punto2[0], punto2[1] );
                
                punto[0] = el.puntoA.getX();
                punto[1] = el.puntoA.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY* (-1));
                punto2 = m2.pprima(punto);
                el.puntoA.setLocation(punto2[0] , punto2[1] );
                
                panel.dibujarFiguras();
            }
      }
      if(key==KeyEvent.VK_RIGHT){
      System.out.println("Trasladar");
            int n = Integer.parseInt(panel.textoNumeroFigura.getText()) - 1;
            Figura f = panel.aFiguras.get(n);

            if (f instanceof Linea) {
                Linea l = (Linea) f;
                panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, panel.panelRaster.getBackground(), false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = 1;
                int transY = 0;

                punto[0] = l.punto1.getX();
                punto[1] = l.punto1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                l.punto1.setLocation(punto2[0], punto2[1]);

                punto[0] = l.punto2.getX();
                punto[1] = l.punto2.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                l.punto2.setLocation(punto2[0], punto2[1]);

                //  panel.dibujarLinea((Point) l.punto1, (Point) l.punto2, l.color, false);
                panel.dibujarFiguras();
            }

            if (f instanceof TrianguloR) {
                TrianguloR t = (TrianguloR) f;
                Point p1 = new Point();
                Point p2 = new Point();
                Point p3 = new Point();
                p1.setLocation(t.v[0].x, t.v[0].y);
                p2.setLocation(t.v[1].x, t.v[1].y);
                p3.setLocation(t.v[2].x, t.v[2].y);
                panel.dibujarTriangulo(p1, p2, p3, Color.WHITE, false);

                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = 1;
                int transY = 0;

                punto[0] = p1.getX();
                punto[1] = p1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                p1.setLocation(punto2[0], punto2[1]);

                punto[0] = p2.getX();
                punto[1] = p2.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                p2.setLocation(punto2[0], punto2[1]);

                punto[0] = p3.getX();
                punto[1] = p3.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                p3.setLocation(punto2[0], punto2[1]);

                Vertex2D v1 = new Vertex2D(p1.x, p1.y, t.color.getRGB());
                Vertex2D v2 = new Vertex2D(p2.x, p2.y, t.color.getRGB());
                Vertex2D v3 = new Vertex2D(p3.x, p3.y, t.color.getRGB());

                TrianguloR tri = new TrianguloR(v1, v2, v3, t.color);
                panel.aFiguras.set(n, tri);
                //  panel.dibujarTriangulo(p1, p2, p3, t.color, false);
                panel.dibujarFiguras();
            }

            if (f instanceof Circulo) {
                Circulo cr = (Circulo) f;
                panel.dibujarCirculo((Point) cr.puntoC, (Point) cr.puntoR, Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = 1;
                int transY = 0;

                punto[0] = cr.puntoC.getX();
                punto[1] = cr.puntoC.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY * (-1));
                punto2 = m.pprima(punto);
                cr.puntoC.setLocation(punto2[0], punto2[1]);

                punto[0] = cr.puntoR.getX();
                punto[1] = cr.puntoR.getY();
                punto[2] = 1;

                punto2 = new double[3];
                punto2 = m.pprima(punto);
                cr.puntoR.setLocation(punto2[0], punto2[1]);
                panel.dibujarFiguras();

            }

            if (f instanceof Cuadrado) {
                Cuadrado cd = (Cuadrado) f;
                panel.dibujarTriangulo(cd.punto1, cd.punto2, cd.punto3,Color.WHITE, false);
                panel.dibujarTriangulo(cd.punto1, cd.punto2, cd.punto4,Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
              int transX = 1;
                int transY = 0;

                

                punto[0] = cd.punto1.getX();
                punto[1] = cd.punto1.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY* (-1));
                punto2 = m.pprima(punto);
                cd.punto1.setLocation(punto2[0], punto2[1]);

                punto[0] = cd.punto2.getX();
                punto[1] = cd.punto2.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY* (-1));
                punto2 = m1.pprima(punto);
                cd.punto2.setLocation(punto2[0], punto2[1] );
                
                punto[0] = cd.punto3.getX();
                punto[1] = cd.punto3.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY* (-1));
                punto2 = m2.pprima(punto);
                cd.punto3.setLocation(punto2[0] , punto2[1] );
                
                punto[0] = cd.punto4.getX();
                punto[1] = cd.punto4.getY();
                punto[2] = 1;

                Matrix m3 = new Matrix();
                m3.traslacion(transX, transY* (-1));
                punto2 = m3.pprima(punto);
                cd.punto4.setLocation(punto2[0] , punto2[1] );

                //    panel.dibujarCuadrado((Point) cd.punto1, (Point) cd.punto2, cd.color, false);
                panel.dibujarFiguras();
            }
            if (f instanceof Elipse) {
                Elipse el = (Elipse) f;
                panel.dibujarElipse((Point) el.puntoC, (Point) el.puntoR,(Point)el.puntoA, Color.WHITE, false);
                double[] punto = new double[3];
                double[] punto2 = new double[3];
                int transX = 1;
                int transY = 0;
                
                

                punto[0] = el.puntoC.getX();
                punto[1] = el.puntoC.getY();
                punto[2] = 1;

                Matrix m = new Matrix();
                m.traslacion(transX, transY* (-1));
                punto2 = m.pprima(punto);
                el.puntoC.setLocation(punto2[0], punto2[1]);

                punto[0] = el.puntoR.getX();
                punto[1] = el.puntoR.getY();
                punto[2] = 1;

                Matrix m1 = new Matrix();
                m1.traslacion(transX, transY* (-1));
                punto2 = m1.pprima(punto);
                el.puntoR.setLocation(punto2[0], punto2[1] );
                
                punto[0] = el.puntoA.getX();
                punto[1] = el.puntoA.getY();
                punto[2] = 1;

                Matrix m2 = new Matrix();
                m2.traslacion(transX, transY* (-1));
                punto2 = m2.pprima(punto);
                el.puntoA.setLocation(punto2[0] , punto2[1] );
                
                panel.dibujarFiguras();
            }
      }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
    }

}
